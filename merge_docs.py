from pathlib import Path
import PyPDF2
import sqlite3
import shutil

# database name to work with
db = 'steuer.db'

# create an connection object
conn = sqlite3.connect(db)

# create a cursor to make statements
c = conn.cursor()

# what's the current path
current = str(Path.cwd().absolute())

GesList = ['4', '5', '6', '8', '9', '10', '12']
DocList = []
c.execute("SELECT kuid, ges4, ges5, ges6, ges8, ges9, ges10 FROM t_kg")
for line in c.fetchall():
    kn = line[0]
    # print(line)
    for i in range(1, 7):
        if line[i] == '1':
            DocList.append(
                './output/EBID_11510_GESID_' + GesList[i - 1] + '/' + str(kn) +
                '.pdf')
    # print(DocList)
    pdfWriter = PyPDF2.PdfFileWriter()
    for file in DocList:
        pdfFile = open(file, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFile)
        for pageNum in range(pdfReader.numPages):
            pageObj = pdfReader.getPage(pageNum)
            pdfWriter.addPage(pageObj)
        pdfFile.close

        pdfOutputFile = open('output_merged/' + str(kn) + '.pdf', 'wb')
        pdfWriter.write(pdfOutputFile)
        pdfOutputFile.close()

    DocList = []

conn.commit()
conn.close()
