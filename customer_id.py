from pathlib import Path
import sqlite3


db = 'steuer.db'

# remove db if exists
if Path(db).is_file():
    Path(db).unlink()

# create an connection object
conn = sqlite3.connect(db)

# create a cursor to make statements
c = conn.cursor()

# create a table to insert all customers
c.execute("""CREATE TABLE t_kg_c (
        kuid integer,
        ges text
    )""")

# commit the statement
conn.commit()
# if this was the last statement close the connection
# conn.close()

# remove all entries from the table
c.execute('DELETE FROM t_kg_c')
conn.commit()

# walk through all the subdirs
current = str(Path.cwd().absolute())
for file in Path.cwd().glob('**/*.pdf'):
    arr = str(file).split('/')
    if arr[7] == 'output':
        c.execute("INSERT INTO t_kg_c VALUES (:first, :second)",
                  {'first': arr[9][:-4], 'second': arr[8]})

# conn.commit()

# create another table
c.execute("""CREATE TABLE t_kg (
    kuid integer,
    ges4 text, ta4 text, se4 integer,
    ges5 text, ta5 text, se5 integer,
    ges6 text, ta6 text, se6 integer,
    ges8 text, ta8 text, se8 integer,
    ges9 text, ta9 text, se9 integer,
    ges10 text, ta10 text, se10 integer,
    ges12 text, ta12 text, se12 integer
     )""")
# conn.commit()

c.execute("INSERT INTO t_kg (kuid) SELECT kuid FROM t_kg_c GROUP BY kuid")
# conn.commit()

# only the directories in square brackets
for i in [4, 5, 6, 8, 9, 10, 12]:
    c.execute(
        "UPDATE t_kg SET ges" +
        str(i) +
        "='1' WHERE kuid IN (SELECT b.kuid FROM t_kg_c b WHERE b.ges LIKE '%GESID_" +
        str(i) +
        "')")

conn.commit()

conn.close()
print('READY')
