import re

str1 = '2002 516.365 27 0,00 0,00 7,22 -7,22'.replace(' ', '')

print(str1)
# str1 = (stra+strb+strc).replace(' ', '')

kn = '105'

pattern1 = re.compile(r'([^,]*,..){4}')
pattern2 = re.compile(r'[\d]*\.\d{3}')
pattern3 = re.compile(r'\-*\d+\,+\d{2}')

matches1 = pattern1.finditer(str1)
tabline = []

for match1 in matches1:

    # KundenNummer
    tabline.append(kn)

    # Jahr
    tabline.append(match1.group(0)[0:4])

    str2 = match1.group(0)[4:]

    matches2 = pattern2.finditer(str2)
    for match2 in matches2:

        # Gesell.Nr
        tabline.append(match2.group(0))
        str3 = str2[match2.span(0)[1]:]

        # EK_ART
        tabline.append(str3[0:1])
        print(str3)

        str4 = str3[1:].replace('.', '')
        matches3 = pattern3.finditer(str4)
        for match3 in matches3:
            # lfd einkünfte > veräusserung > sonderausg > gesamt
            tabline.append(match3.group(0))

        if tabline[0] == kn:
            print(tabline)
        tabline = []
