import PyPDF2

inputFile = 'EBID_11510_GESID_6.pdf'
outFolder = 'EBID_11510_GESID_6'
pgStart = 0
pgStop = 0
kn = 0

try:
    pdfFileObj = open('input/'+inputFile, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    pdfWriter = PyPDF2.PdfFileWriter()

    for i in range(0, pdfReader.numPages):
        pageObj = pdfReader.getPage(i)
        str = pageObj.extractText()
        if 'Kundennr:' in str:
            index = str.rfind('Kundennr:')
            subarr = str[index+9:].split('IdNr:')

            # hier findet das trennen des Dokuments statt
            if kn != subarr[0] and pgStop != 0:
                pdfWriter = ""
                pdfWriter = PyPDF2.PdfFileWriter()

                for pageNum in range(pgStart, pgStop+1):
                    pageObjW = pdfReader.getPage(pageNum)
                    pdfWriter.addPage(pageObjW)

                pdfOutputFile = open('output/'+outFolder+'/'+kn+'.pdf', 'wb')
                pdfWriter.write(pdfOutputFile)
                pdfOutputFile.close()

            kn = subarr[0]  # Kundennummer
            pgStart = i  # PageStart
            pgStop = i
        else:
            pgStop += 1

    pdfFileObj.close

except FileNotFoundError as error:
    print(error)
