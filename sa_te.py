from pathlib import Path
import sqlite3
import PyPDF2
from datetime import datetime

# database name to work with
db = 'steuer.db'
# 4, 5, 6, 8, 9, 10, 12
n = '12'

# create an connection object
conn = sqlite3.connect(db)

# create a cursor to make statements
c = conn.cursor()

# what's the current path
current = str(Path.cwd().absolute())

# walk through all the subdirs
for file in Path.cwd().glob('output/EBID_11510_GESID_'+n+'/*.pdf'):
    try:
        pdfFileObj = open(file, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        pdfWriter = PyPDF2.PdfFileWriter()

        for i in range(0, pdfReader.numPages):
            pageObj = pdfReader.getPage(i)
            str = pageObj.extractText()
            if 'Tag der Auszahlung ' in str:
                index = str.rfind('Tag der Auszahlung ')
                subarr = str[index+19:].split('. Gemäß')
                kn = file.stem
                d = datetime.strptime(subarr[0].strip(), '%d.%m.%Y')
                datum = d.strftime('%Y-%m-%d')

        # reset variables
        subarr = None
        str = None

        for i in range(0, pdfReader.numPages):
            pageObj = pdfReader.getPage(i)
            str = pageObj.extractText()
            if 'Steuer auf Einkünfte i. H. v.' in str:
                index = str.rfind('Steuer auf Einkünfte i. H. v.')
                subarr = str[index+29:].split('• zu Grunde')
                betrag = subarr[0].strip()

                c.execute(
                    "UPDATE t_kg SET ta" + n + "=:datum, se" + n +
                    "=:betrag WHERE kuid=:kn",
                    {'datum': datum, 'betrag': betrag, 'kn': kn})
                conn.commit()
        pdfFileObj.close
    except FileNotFoundError as error:
        print(error)
print(datetime.now().time())
