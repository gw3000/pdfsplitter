# Howto split a String with the use of RegEx

source:
* 2002636.03080,00306,800,00306,802002636.03010,0059,66-6,2665,92

## regular expression to split the string in the year 2002
`
([^,]*,..){4}
`

result:
* 2002636.03080,00306,800,00306,80
* 2002636.03010,0059,66-6,2665,92

## regex first four number
`
\d{4}
`

result:
* 2002
* 636.03010,0059,66-6,2665,92

## regex next number
`
\d{3}\.\d{3}
`

result:
* 636.030
* 10,0059,66-6,2665,92

## regex to split into money
`
(-|[0-9])*\,[0-9]{2}
`

result:
* 10,00
* 59,66
* -6,26
* 65,92
