from pathlib import Path
import PyPDF2
import shutil

# what's the current path
current = str(Path.cwd().absolute())

# walk through all the subdirs
for n in ['4', '5', '6', '8', '9', '10', '12']:
    for file in Path.cwd().glob('output/EBID_11510_GESID_'+n+'/*.pdf'):
        try:
            pdf1File = open(file, 'rb')
            pdf2File = open('input/empty.pdf', 'rb')
            pdf1Reader = PyPDF2.PdfFileReader(pdf1File)
            pdf2Reader = PyPDF2.PdfFileReader(pdf2File)
            pdfWriter = PyPDF2.PdfFileWriter()

            pageNum = pdf1Reader.getNumPages()
            if pageNum % 2 != 0:
                print(file)

                for pageNum in range(pdf1Reader.numPages):
                    pageObj = pdf1Reader.getPage(pageNum)
                    pdfWriter.addPage(pageObj)

                for pageNum in range(pdf2Reader.numPages):
                    pageObj = pdf2Reader.getPage(pageNum)
                    pdfWriter.addPage(pageObj)

                pdfOutputFile = open(file.stem+'.pdf', 'wb')
                pdfWriter.write(pdfOutputFile)
                pdfOutputFile.close()

                # move the evened file to its source
                shutil.move(file.stem+'.pdf', file)

            pdf1File.close
            pdf2File.close
        except FileNotFoundError as error:
            print(error)
