from pathlib import Path
import sqlite3
import PyPDF2
import re

# regular expressions
pattern1 = re.compile(r'([^,]*,..){4}')
pattern21 = re.compile(r'[\d]*\.\d{3}')
pattern22 = re.compile(r'\d{3}')
pattern3 = re.compile(r'\-*\d+\,+\d{2}')

# database to work with
db = 'steuer.db'

# create an connection object
conn = sqlite3.connect(db)

# create a cursor to make statements
c = conn.cursor()

# create table
c.execute("""DROP TABLE IF EXISTS t_betr;""")

c.execute("""CREATE TABLE t_betr (
     kn INTEGER NOT NULL,
     jahr INTEGER NOT NULL,
     gesellnr INTEGER NOT NULL,
     ekart INTEGER NOT NULL,
     lfdek NUMERIC NOT NULL,
     vergew NUMERIC NOT NULL,
     sbausgaben NUMERIC NOT NULL,
     gesamt NUMERIC NOT NULL,
     pfad INTEGER NOT NULL
 );""")

# commit statement
conn.commit()

# what's the current path
current = str(Path.cwd().absolute())

# GesList = ['4', '5', '6', '8', '9', '10', '12']
GesList = ['X']

for n in GesList:
    for file in Path.cwd().glob('output/EBID_11510_GESID_' + n + '/*.pdf'):
        try:
            pdfFileObj = open(file, 'rb')
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
            pdfWriter = PyPDF2.PdfFileWriter()
            fstring = 'JAHRGesell.NrEK_ARTLfd.EinkünfteVeräußerungs-gewinneSonderbetriebs-ausgabenGesamt'
            kn = file.stem

            for i in range(0, pdfReader.numPages):
                pageObj = pdfReader.getPage(i)
                strg = pageObj.extractText()
                if fstring in strg:
                    index = strg.rfind(fstring)
                    str1 = strg[index + 81:].replace(' ', '')
                    matches1 = pattern1.finditer(str1)
                    tabline = []

                    for match1 in matches1:

                        # KundenNummer
                        tabline.append(kn)

                        # Jahr
                        tabline.append(match1.group(0)[0:4])

                        str2 = match1.group(0)[4:]

                        # gesellnr with or without point (pat21 & pat22)
                        if str2[0:5].find('.') > 0:
                            matches2 = pattern21.finditer(str2)
                        else:
                            matches2 = pattern22.finditer(str2)

                        for match2 in matches2:

                            # Gesell.Nr
                            tabline.append(match2.group(0))
                            str3 = str2[match2.span(0)[1]:]

                            # EK_ART
                            tabline.append(str3[0:1])

                            str4 = str3[1:].replace('.', '')
                            matches3 = pattern3.finditer(str4)
                            for match3 in matches3:
                                # lfd einkünfte > veräusserung > sonderausg >
                                # gesamt
                                tabline.append(match3.group(0))

                            if tabline[0] == kn:
                                # print(tabline)
                                c.execute(
                                    "INSERT INTO t_betr (kn,jahr,gesellnr,ekart,lfdek,vergew,sbausgaben,gesamt, pfad) VALUES ('" +
                                    tabline[0] +
                                    "','" +
                                    tabline[1] +
                                    "','" +
                                    tabline[2] +
                                    "','" +
                                    tabline[3] +
                                    "','" +
                                    tabline[4] +
                                    "','" +
                                    tabline[5] +
                                    "','" +
                                    tabline[6] +
                                    "','" +
                                    tabline[7] +
                                    "','" + n + "');")
                                conn.commit()
                            tabline = []

                    pdfFileObj.close

        except FileNotFoundError as error:
            print(error)
